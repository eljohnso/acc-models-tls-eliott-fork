# MadX expression     LSA name
bsw42_x_mm_abs        PSBEAM/BSW42_ABS_POSITION_X_MM.INJ1
bsw42.2_x_mm_abs      PSBEAM/BSW42_ABS_POSITION_X_MM.INJ2
smh26_x_mm_abs        PSBEAM/SMH26_ABS_POSITION_X_MM
table(summ,q1)        PSBEAM/QX_MODEL
table(summ,q2)        PSBEAM/QY_MODEL
# optics at the wire scanners
table(twiss,PR.BWSH54,BETX)                  PSBWS_OPTICS/Twiss#pr_bwsh54_beta            
table(twiss,PR.BWSH65,BETX)                  PSBWS_OPTICS/Twiss#pr_bwsh65_beta            
table(twiss,PR.BWSH68,BETX)                  PSBWS_OPTICS/Twiss#pr_bwsh68_beta            
table(twiss,PR.BWSV64,BETY)                  PSBWS_OPTICS/Twiss#pr_bwsv64_beta            
table(twiss,PR.BWSV85,BETY)                  PSBWS_OPTICS/Twiss#pr_bwsv85_beta            
table(twiss,PR.BWSH54,DX)*beam->beta         PSBWS_OPTICS/Twiss#pr_bwsh54_disp            
table(twiss,PR.BWSH65,DX)*beam->beta         PSBWS_OPTICS/Twiss#pr_bwsh65_disp            
table(twiss,PR.BWSH68,DX)*beam->beta         PSBWS_OPTICS/Twiss#pr_bwsh68_disp            
table(twiss,PR.BWSV64,DY)*beam->beta         PSBWS_OPTICS/Twiss#pr_bwsv64_disp            
table(twiss,PR.BWSV85,DY)*beam->beta         PSBWS_OPTICS/Twiss#pr_bwsv85_disp 
# optics at the BGIs
table(twiss,PR.BGI82,BETX)                   PSBGI_OPTICS/Twiss#pr_bgi82_beta
table(twiss,PR.BGI84,BETY)                   PSBGI_OPTICS/Twiss#pr_bgi84_beta
table(twiss,PR.BGI82,DX)*beam->beta          PSBGI_OPTICS/Twiss#pr_bgi82_disp
table(twiss,PR.BGI84,DY)*beam->beta          PSBGI_OPTICS/Twiss#pr_bgi84_disp          
