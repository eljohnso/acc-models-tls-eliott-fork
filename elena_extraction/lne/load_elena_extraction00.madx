/******************************************************************
 **
 ** ELENA extraction => settings taken from Davide Gamba's files
 **
 **  M. Fraser and F. Velotti: caluclating initial conditions for LNE00
 ******************************************************************/

option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/

! Path needs updating when moving into repository

option, -warn;
call, file = "elena_repo/elements/ELENA_elements.def";
call, file = "elena_repo/sequence/ELENA_ring.seq";

/*****************************************************************************
 * Set quadrupolar strength as given during design
 *****************************************************************************/
 !!! Qx=2.3,Qy=1.3, gap=76mm, E1=E2=Pi*17/180, FINT=0.424, Lbm=0.927m
 KQ1:= 2.27646e+00;
 KQ2:=-1.20793e+00;
 KQ3:= 7.19841e-01;


/******************************************************************
 * Macros
 ******************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname, filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of ELENA extraction to LNE00";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};

/*******************************************************************************
 * beam
 *******************************************************************************/

 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! Assuming extraction energy of 100 keV

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 
 /******************************************************************
 * Twiss, determine extraction location and install marker
 ******************************************************************/
 
 use, sequence=elena;
 twiss;

 sExtraction00 = table(twiss,LNR.ZDFHL.0610,s) - LNR.ZDFHL.0610->l/2 - 0.3;
 
 pointExtraction00: marker;
 
 SEQEDIT, SEQUENCE=ELENA;
       	FLATTEN;
        INSTALL, ELEMENT=pointExtraction00, AT= sExtraction00, FROM=ELENA$START;
        FLATTEN;
 ENDEDIT;
 
use, sequence=elena;
savebeta, label=tl_initial_cond, place = pointExtraction00;
savebeta, label=ring_initial_cond, place = ELENA$START;
twiss;

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/

exec, write_ini_conditions(0,0,ring_initial_cond,lnr_start.inp);

EXTRACT, sequence=ELENA, FROM=ELENA$START, TO=pointExtraction00, newname=ELENA_EXTRACT;

use, sequence=elena_extract;
twiss, beta0=ring_initial_cond;

calculate_extraction(delta_kicker, sign, ring_twiss_file) : macro = {
    
    if(sign == 1){
        kick_s = 1.0;
    }   
    else{
        kick_s = -1.0;
    };
    

    create,table=trajectory, column=_NAME,S,L, _KEYWORD, BETX,ALFX, x, px, dx, dpx, MUX,BETY,ALFY,Y,DY,PY,DPY,MUY, k1l;
    use, sequence = ELENA_EXTRACT;
    twiss, beta0 = ring_initial_cond, table=twiss_nom;

    value, delta_kicker;  
    LNR.ZDFA.0310, kick = delta_kicker * kick_s;
    
    use, sequence = ELENA_EXTRACT;

    select, flag = twiss, clear;
    savebeta,label=tl_initial_cond, place = pointExtraction00;
    SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX, X, DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
    twiss, beta0 = ring_initial_cond;

    len_twiss = table(twiss_nom, tablelength);
    value, len_twiss;
    
    i = 2;
    option, -info;
    while(i < len_twiss){

        SETVARS, TABLE=twiss_nom, ROW=i;
        x0 = x;
        px0 = px;
        SETVARS, TABLE=twiss, ROW=i;
        x = x - x0;
        px = px - px0;

        fill, table=trajectory;

        i = i + 1;
    };

    write, table=trajectory, file="ring_twiss_file";
    
    betx0 = tl_initial_cond->betx;
    bety0 =  tl_initial_cond->bety;

    alfx0 = tl_initial_cond->alfx;
    alfy0 = tl_initial_cond->alfy;

    dx0 = tl_initial_cond->dx;
    dy0 = tl_initial_cond->dy;

    dpx0 = tl_initial_cond->dpx;
    dpy0 = tl_initial_cond->dpy;

    x0 = tl_initial_cond->x;
    y0 = tl_initial_cond->y;

    px0 = tl_initial_cond->px;
    py0 = tl_initial_cond->py;

    mux0 = tl_initial_cond->mux;
    muy0 = tl_initial_cond->muy;

};