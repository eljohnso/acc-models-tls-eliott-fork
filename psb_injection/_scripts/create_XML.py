# to create XML code
from yattag import Doc, indent
import glob

energy_spread = '100kev'
#energy_spread = '250kev'
#energy_spread = '450kev'
l4t_to_bi_optic = 'PSBInjection_2021_' + energy_spread 
default_optic = l4t_to_bi_optic + '_qx_4.400_qy_4.450'
default_sequence = 'l4tltltbbi3psb3'


# assuming to launch it from _scripts folder
folder = '../jmad/'
filename = folder + 'operation/l4t_lt_ltb_bipsb_' + energy_spread + '_stitched.jmd.xml' 



tune_control_files = sorted(glob.glob(folder + 'psb_tune_control/*.str'))

doc, tag, text = Doc().tagtext()

with tag('jmad-model-definition', name = 'PsbInjection_'+energy_spread):
    
    # define different optics via their strengths files
    with tag('optics'):
        for file_ in tune_control_files:
            psb_tune = file_.split('/')[-1][:-4].replace('psb_inj','') 
            optic = l4t_to_bi_optic + psb_tune
            #print(optic)
            with tag('optic', name = optic, overlay = 'false'):
                with tag('init-files'):
                    doc.stag('call-file', path = '../' + file_)
                with tag('post-ptc-files'):
                    doc.stag('call-file', path = './ptc_macro.madx')

    doc.stag('default-optic ref-name="' + default_optic + '"')
    
    # define the sequence
    with tag('sequences'):
        for i in range(4):
            sequence = 'l4tltltbbi' + str(i+1) + 'psb' + str(i+1)
            with tag('sequence', name=sequence):
                with tag('ranges'):
                    with tag('range', name='ALL'):
                        doc.stag('madx-range', first='#s', last='#e')
                        with tag('twiss-initial-conditions', name=sequence+'-ALL-twiss'):
                            doc.stag('ptc-icase',    value="5")
                            doc.stag('ptc-no',       value="5")
                            doc.stag('ptc-betz',     value="0.1")
                            doc.stag('chrom',        value="true")
                            doc.stag('closed-orbit', value="false")
                            doc.stag('centre',       value="true")
                            doc.stag('betx',         value="5.5270")
                            doc.stag('bety',         value="11.0314")
                            doc.stag('alfx',         value="1.7357")
                            doc.stag('alfy',         value="-3.0024")
                            doc.stag('dx',           value="0.000")
                            doc.stag('dpx',          value="0.000")
                            doc.stag('dy',           value="0.000")
                            doc.stag('dpy',          value="0.000")
                        with tag('post-use-files'):
                            doc.stag('call-file', path='seterrtab' + str(i+1) + '.madx')
                doc.stag('default-range ref-name="ALL"')

        with tag('sequence', name='l4tltltblbe'):
            with tag('ranges'):
                with tag('range', name='ALL'):
                    doc.stag('madx-range', first='#s', last='#e')
                    with tag('twiss-initial-conditions', name='l4tltltblbe-ALL-twiss'):
                        doc.stag('ptc-icase',    value="5")
                        doc.stag('ptc-no',       value="5")
                        doc.stag('ptc-betz',     value="0.1")
                        doc.stag('chrom',        value="true")
                        doc.stag('closed-orbit', value="false")
                        doc.stag('centre',       value="true")
                        doc.stag('betx',         value="5.5270")
                        doc.stag('bety',         value="11.0314")
                        doc.stag('alfx',         value="1.7357")
                        doc.stag('alfy',         value="-3.0024")
                        doc.stag('dx',           value="0.000")
                        doc.stag('dpx',          value="0.000")
                        doc.stag('dy',           value="0.000")
                        doc.stag('dpy',          value="0.000")
            doc.stag('default-range ref-name="ALL"')


    doc.stag('default-sequence ref-name="' + default_sequence + '"')
    
    with tag('init-files'):
        for i in range(4):
            sequence = 'l4t_lt_ltb_bi' + str(i+1) + 'psb' + str(i+1)
            doc.stag('call-file',  path='../../lhc/stitched/jmad/' + sequence + '_' + energy_spread + '_savedseq.seq')
            doc.stag('call-file',  path='../../lhc/stitched/jmad/' + sequence + '_' + energy_spread + '_ref_change.seq')
            doc.stag('table-file', path='../../lhc/stitched/jmad/' + sequence + '_' + energy_spread + '_errors.seq', **{'table-name':'errtab'+ str(i+1)} ) 

        doc.stag('call-file', path = '../../lhc/stitched/jmad/lbe_seq_' + energy_spread + '_opt_savedseq.seq')


    with tag('path-offsets'):
        doc.stag('repository-prefix', value='./')
        doc.stag('resource-prefix', value='./')

result = indent(doc.getvalue(), indentation = ' '*2, newline = '\r\n')

with open(filename, 'w') as f:
    print(result, file = f)

#print(result)
    
print('XML code written to file ' + filename )
