/******************************************************************************************
 *
 * MAD-X input script for the injection optics
 *
 * 08/05/2020 - Fanouria Antoniou, Hannes Bartosik, Chiara Bracco, 
 * Gian Piero di Giovanni, Alexander Huschauer, Elisabeth Renner

 * 01/09/2020 - C.Bracco  => stitched model created using PTC
 * 02/09/2020 - F.Velotti => fix for JMAD and proper PSB calling
 * 02/09/2020 - F.Velotti => saving sequence and check
 * 02/09/2020 - F.Velotti => groupping all BI stuff and dividing files using templates
 * 22/01/2021 - F.Velotti => moving to single source file that uses any tune in the PSB
 ******************************************************************************************/

make_ptctwiss_stitched(_ring_, _bunch_sett_) : macro = {

    ptc_create_universe;
    ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
    SELECT, FLAG = ptc_twiss, CLEAR;
    select, flag=ptc_twiss, column=NAME,KEYWORD,ANGLE,K1L,S,L,BETX,BETY,DISP1,DISP3,X,Y,PX,PY,ALFX,ALFY,DISP2,DISP4,E1,E2,MU1,MU2,APER_1,APER_2;
    ptc_align;
    ptc_eplacement, range=bi_ring_.tstr1l1, onlyposition, x=xr0_ring_, y=yr0_ring_, z=0, refframe=previouselement, autoplacedownstream=true;
    ptc_twiss, icase=5,  no=5, table = ptc_twiss, summary_table = ptc_twiss_summary, normal,  beta0=initbeta0 , 
    file = twiss_bi_ring_psb_ring___bunch_sett_kev.tfs;
    ptc_end;

};

make_ptctwiss_glued(_ring_, _bunch_sett_) : macro = {

    ptc_create_universe;
    ptc_create_layout, time=false, model=2, exact=true, method=6, nst=3;
    SELECT, FLAG = ptc_twiss, CLEAR;
    select, flag=ptc_twiss, column=NAME,KEYWORD,ANGLE,K1L,S,L,BETX,BETY,DISP1,DISP3,X,Y,PX,PY,ALFX,ALFY,DISP2,DISP4,E1,E2,MU1,MU2,APER_1,APER_2;
    ptc_align;
    ptc_eplacement, range=bi_ring_.tstr1l1, onlyposition, x=xr0_ring_, y=yr0_ring_, z=0, refframe=previouselement, autoplacedownstream=true;
    ptc_twiss, icase=5,  no=5, table = ptc_twiss, summary_table = ptc_twiss_summary, normal,  beta0=l4t.initbeta0 , 
    file = twiss_l4t_lt_ltb_bi_ring_psb_ring___bunch_sett_kev.tfs;
    ptc_end;

};

/* system, "ln -nfs ./../.. main_dir"; */
/* call, file = "main_dir/templates/optics_convention.cmd"; */
/* call, file = "main_dir/templates/buncher_settings.cmd"; */



call, file = "main_dir/templates/main_template.cmd";

exec, call_psb_str($qx_int, $qx_frac, $qy_int, $qy_frac);

! Just using this variable as numerical value
deb_value = db_set;


/************ Ring 1 ************/

exec, make_l4t_2_ltb_sequence();

call, file = "main_dir/templates/make_stitched_b1.cmd";
exec, assign_errors_psb1();

! Arguments = ring, debuncher settings
exec, make_ptctwiss_stitched(1, $deb_value);

! Long glued model (wrong optics!!!!!!)
exec, make_l4t_2_psb(1);
exec, assign_errors_psb1();

! Arguments = ring, debuncher settings
exec, make_ptctwiss_glued(1, $deb_value);

! Argument = ring
exec, prepare_to_save_glued(1);

! Arguments = debuncher settings, ring
exec, save_sequence_glued($deb_value, 1);


/************ Ring 2 ************/
call, file = "main_dir/templates/make_stitched_b2.cmd";
exec, assign_errors_psb2();

exec, make_ptctwiss_stitched(2, $deb_value);

! Long glued model (wrong optics!!!!!!)
exec, make_l4t_2_psb(2);
exec, assign_errors_psb2();

! Arguments = ring, debuncher settings
exec, make_ptctwiss_glued(2, $deb_value);

! Argument = ring
exec, prepare_to_save_glued(2);

! Arguments = debuncher settings, ring
exec, save_sequence_glued($deb_value, 2);


/************ Ring 3 ************/

call, file = "main_dir/templates/make_stitched_b3.cmd";
exec, assign_errors_psb3();

exec, make_ptctwiss_stitched(3, $deb_value);

if (deb_value == 100){

    write, table = ptc_twiss, file = "twiss_bi3psb3_100kev_nom.tfs";

};

! Long glued model (wrong optics!!!!!!)
exec, make_l4t_2_psb(3);
exec, assign_errors_psb3();

! Arguments = ring, debuncher settings
exec, make_ptctwiss_glued(3, $deb_value);

! Argument = ring
exec, prepare_to_save_glued(3);

! Arguments = debuncher settings, ring
exec, save_sequence_glued($deb_value, 3);


/************ Ring 4 ************/

call, file = "main_dir/templates/make_stitched_b4.cmd";
exec, assign_errors_psb4();

exec, make_ptctwiss_stitched(4, $deb_value);

! Long glued model (wrong optics!!!!!!)
exec, make_l4t_2_psb(4);
exec, assign_errors_psb4();

! Arguments = ring, debuncher settings
exec, make_ptctwiss_glued(4, $deb_value);

! Argument = ring
exec, prepare_to_save_glued(4);

! Arguments = debuncher settings, ring
exec, save_sequence_glued($deb_value, 4);


/************ LBE ************/

exec, save_sequence_lbe($deb_value);

/************************************
* Cleaning up
************************************/
system, "rm psb_repo";
system, "rm -rf psb_repo";
system, "rm internal_mag_pot.txt";
system, "rm main_dir";
system, "rm bi_repo";
system, "rm linac_repo";
system, "rm -rf linac_repo";

