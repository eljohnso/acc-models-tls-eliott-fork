To generate website with new TL folder:
 - run 
```python site_creation_tls_new.py <path_to_acc-models-www>```
    - ```path_to_acc-models-www``` is the path to pass as argument, for example: 
    ``` python site_creation_tls_new.py /home/fvelotti/acc_optics_db/```
 - Then go back to the root of acc-models-www and from there run: ```python website_creation/create_mkdocs_config_file.py```
