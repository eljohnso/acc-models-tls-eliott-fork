!==============================================================================================
! MADX file for LNS-LNI optics
!
! M.A. Fraser, D. Gamba, F.M. Velotti
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "LNS-LNI optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../../elena_extraction/lne lne_repo";
system, "ln -fns ./../../delnslni delnslni_repo";

/*******************************************************************************
 * beam
 *******************************************************************************/
 beam, particle=antiproton;

 mass=beam->mass;

 Ekin=0.0001; ! 100 Kev

 gamman=(Ekin/mass)+1;
 beta=sqrt(-((1/gamman)^2)+1);
 value,beta;
 pcn=sqrt((mass^2)*((gamman^2)-1));
 
 beam, particle=antiproton,pc=pcn,exn=6E-6,eyn=4E-6;
 
/*****************************************************************************
 * Load element R matrix definition
 *****************************************************************************/
 call, file = "lne_repo/deflectors.ele";


/*****************************************************************************
 * LNS
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/lns/lns.ele";
 call, file = "delnslni_repo/lns/lns_lni_k.str";
 call, file = "delnslni_repo/lns/lns.seq";
 !call, file = "delnslni_repo/lns/lns.dbx"; !Presently no aperture database: to be updated
 option, echo;
 
 /*****************************************************************************
 * LNI
 *
 *****************************************************************************/
 option, -echo;
 call, file = "delnslni_repo/lni/lni.ele";
 call, file = "delnslni_repo/lni/lni.seq";
 !call, file = "delnslni_repo/lni/lni.dbx"; !Presently no aperture database: to be updated
 option, echo; 
 
/*******************************************************************************
 * build up the geometry of the beam lines and select a line
 *******************************************************************************/
 lnslni: sequence, refer=ENTRY, l = 2.8264 + 4.6136;
   lns                    , at =      0;
   lni_b                  , at = 2.8264;
 endsequence;

 SEQEDIT, SEQUENCE=lnslni; FLATTEN; ENDEDIT;
 

/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "../stitched/elena_source.inp";


! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();

/*******************************************************************************
! set matched twiss parameters
 *******************************************************************************/
 call, file = "../stitched/elena_matched.inp";
 
set_final_conditions() : macro = {

    INITBETA1: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_final_conditions();

 /*******************************************************************************
 * MATCH
 *******************************************************************************

! Limits for k of quads assuming hard-edge model for a voltage of 6.5kV: 
! LNE = 72.2 (length = 100 mm, electrode radius = 30 mm)
! LNS = 103.9 (length = 70 mm, electrode radius = 25 mm)

 lnslnimatch: macro={
select,flag=twiss,column=name,s,betx,bety,alfx,alfy,dx,dpx,dy,dpy,mux,muy,l,k0l,k1l;
OPTION, sympl = false;
twiss, beta0=initbeta0, file = "twiss_lns_lni_match.tfs";
};

use, sequence= lnslni;
match,vlength=false,use_macro;

emith = 6 ;
emitv = 4 ;
dppi = 2.5;

vary , NAME=KLNS.ZQSF.0001,step=1e-6,lower=-103,upper=103;
vary , NAME=KLNS.ZQSD.0002,step=1e-6,lower=-103,upper=103;

vary , NAME=KLNS.ZQMF.0020,step=1e-6,lower=-72,upper=72;
vary , NAME=KLNS.ZQMD.0021,step=1e-6,lower=-72,upper=72;
vary , NAME=KLNI.ZQMD.0050,step=1e-6,lower=-72,upper=72;
vary , NAME=KLNI.ZQMF.0051,step=1e-6,lower=-72,upper=72;

use_macro,name=lnslnimatch;

alfxM = INITBETA1->alfx;
betxM = INITBETA1->betx;
alfyM = INITBETA1->alfy;
betyM = INITBETA1->bety;
dD := (table(twiss,LNI.END,dx) - INITBETA1->dx)*beta;
dDP := (table(twiss,LNI.END,dpx) - INITBETA1->dpx)*beta;

MMbetax := 0.5*(table(twiss,LNI.END,betx)*(1+alfxM^2)/betxM + betxM*(1+table(twiss,LNI.END,alfx)^2)/table(twiss,LNI.END,betx) - 2*alfxM*table(twiss,LNI.END,alfx));

MMbetay := 0.5*(table(twiss,LNI.END,bety)*(1+alfyM^2)/betyM + betyM*(1+table(twiss,LNI.END,alfy)^2)/table(twiss,LNI.END,bety) - 2*alfyM*table(twiss,LNI.END,alfy));

MMDx := 1 + 0.5*(dD^2 + (betxM*dDP + alfxM*dD)^2)*dppi^2/(betxM*emith/6/beta/gamman);

! Matching constraints for mismatch factors
constraint, weight=1,range=#e, expr = MMbetax = 1;
constraint, weight=1,range=#e, expr = MMbetay = 1;
constraint, weight=1,range=#e, expr = MMDx = 1;

! Matching contraints for Twiss parameters
!constraint, weight=1,range=#e, alfx = INITBETA1->alfx;
!constraint, weight=1,range=#e, alfy = INITBETA1->alfy;
!constraint, weight=1,range=#e, betx = INITBETA1->betx;
!constraint, weight=1,range=#e, bety = INITBETA1->bety;
!constraint, weight=1,range=#e, dx = INITBETA1->dx;
!constraint, weight=1,range=#e, dpx = INITBETA1->dpx;

simplex,calls=10000,tolerance=1e-12;

endmatch;

*******************************************************************************/
  


/*******************************************************************************
 * Twiss
 *******************************************************************************/
 
use, sequence= lns;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lns.tfs";

use, sequence= lnslni;  
OPTION, sympl = false;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_lns_lni_nom.tfs";


/******************************************************************************
* Survey - WARNING: first generate LNS survey file
*******************************************************************************/
 set_su_ini_conditions(yy,xx) : macro = {

 x00 = table(yy,xx,X);
 y00 = table(yy,xx,Y);
 z00 = table(yy,xx,Z);
 theta00 = table(yy,xx,THETA);
 phi00 = table(yy,xx,PHI);
 psi00 = table(yy,xx,PSI);

 };
 
 call, file = "./make_survey_lns_lni.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm lne_repo";
system, "rm delnslni_repo";

stop;
