!==============================================================================================
! MADX file for LHC Q20 to D3 optics
!
! F.M. Velotti, M.A. Fraser
! based on version from AFS repository from O. Berrig and E. Benedetto (2007)
!
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT2 D3 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt2 tt2_repo";
system, "ln -fns ./../../tt2tt10_lhc_q20/stitched lhc_repo";
system, "ln -fns ./../../tt2tt10_lhc_q20/line lhc_line_repo";


/*****************************************************************************
 * TT2
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 option, -echo;
 call, file = "tt2_repo/tt2.ele";
 call, file = "lhc_line_repo/tt2_fe_26.str";
 call, file = "tt2_repo/tt2.seq";
 call, file = "tt2_repo/tt2.dbx";
 option, echo;


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./lhc_repo/tt2_tt10_lhc_q20_from_stitched_kickers.inp";



/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * beam
 *******************************************************************************/
 Beam, particle=PROTON,pc=26,exn=10.0E-6,eyn=5.0E-6;
 use, sequence= tt2d3;


/*******************************************************************************
 * twiss
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_tt2_d3_lhc_q20_nom.tfs";

/*******************************************************************
* Saving sequence for JMAD
*******************************************************************/

system, "rm ./jmad/*";
system, "mkdir jmad";

 set, format="22.10e";
option, -warn;
save, sequence = tt2d3, file="./jmad/tt2_d3_lhc_q20_savedseq.seq", beam;
option, warn;


/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="./jmad/tt2_d3_lhc_q20.inp";

betx0 = initbeta0->betx;
bety0 = initbeta0->bety;
alfx0 = initbeta0->alfx;
alfy0 = initbeta0->alfy;
dx0   = initbeta0->dx;
dy0   = initbeta0->dy;
dpx0  = initbeta0->dpx;
dpy0  = initbeta0->dpy;

print, text="/*********************************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction';
print, text="*********************************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;

/***********************************
* Cleaning up
***********************************/
system, "rm tt2_repo";
system, "rm lhc_repo";
system, "rm lhc_line_repo";

stop;
