!==============================================================================================
! MADX file for TT20-T2 SFTPRO optics
!
! F.M. Velotti
! based on version from AFS repository and from LSA database
!
!==============================================================================================
option, RBARC=FALSE;
option, echo;

 title, "TT20/T2 SFTPRO optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../tt20t2 tt20_repo";

/***************************************
* Make model
***************************************/

call, file = "tt20_repo/tt20t2.seq";
call, file = "./t2_protons_SE_qsplit.str";


/*******************************************************************************
! set initial twiss parameters
 *******************************************************************************/
 call, file = "./tt20_initial_conditions.inp";


/*****************************************************************************
 * store initial parameters in memory block
 *****************************************************************************/
X0=0;
PX0=0;
Y0=0;
PY0=0;
PT0=0;

! All the other initial values are taken from the file called above

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


/*******************************************************************************
 * beam
 *******************************************************************************/

Beam, particle=proton, pc=400;


/*******************************************************************************
 * twiss
 *******************************************************************************/

use, sequence = tt20t2;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt20t2_sftpro_nom.tfs";

/*******************************************************************************
 * twiss with correct optics after splitters using P. Arrutia tracking results
 *******************************************************************************/

! Start TT20 to start 1st splitter
use, sequence = tt20t2, range=#s/ENDTT21;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt20tt21_sftpro.tfs";


! End 1st splitter to start 2nd splitter
call, file = "./tt22_initial_conditions.inp";
exec, set_ini_conditions();
use, sequence = tt20t2, range=BEGTT22/ENDTT22;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt21tt22_sftpro.tfs";

! End 2nd splitter to T2
call, file = "./tt23_initial_conditions.inp";
exec, set_ini_conditions();
use, sequence = tt20t2, range=BEGTT23/ENDTT23;

select, flag=twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "./twiss_tt22tt23_sftpro.tfs";

/*******************************************************************************
 * Survey using initial GEODES values
 *******************************************************************************/

use, sequence = tt20t2, range=#s/ENDTT21;
select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey, X0=-581.39261, Y0=2401.83364, Z0=3722.15603, THETA0=-5.259018*twopi/400, PHI0=-0.00023610, PSI0=0;
write, table=survey, file="./survey_tt20tt21.tfs";


use, sequence = tt20t2, range=BSGV.220075S/ENDTT22;
select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey, X0=-643.37074, Y0=2441.67984, Z0=4369.82595, THETA0=-7.419830*twopi/400, PHI0=-0.00036400, PSI0=0;
write, table=survey, file="./survey_tt21tt22.tfs";


use, sequence = tt20t2, range=BSGV.230111S/ENDTT23;
select, flag=survey, column=name,s,l,angle,x,y,z,theta,phi,psi,globaltilt,slot_id;
survey, X0=-654.69405, Y0=2441.66940, Z0=4466.53953, THETA0=-7.419839*twopi/400, PHI0=-0.00036400, PSI0=0;
write, table=survey, file="./survey_tt22tt23.tfs";

/***********************************
* Cleaning up
***********************************/
system, "rm tt20_repo";

