/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Tt24T4ModelDefinitionFactory implements ModelDefinitionFactory{
    
    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        definitionSet.add(new OpticsDefinitionImpl("TT24T4-SFTPRO-2021v1", new CallableModelFileImpl(
                "operation/beta0.inp", ModelFileLocation.REPOSITORY),new CallableModelFileImpl(
                "tt24t4/tt24t4.seq", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl(
                        "tt24t4_sftpro/line/tt24_initial_conditions.inp", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl("tt24t4_sftpro/line/t4_protons_SE_noqsplit.str", ModelFileLocation.REPOSITORY,
                        ParseType.STRENGTHS),
                new CallableModelFileImpl("tt24t4_sftpro/line/tt24.beam", ModelFileLocation.REPOSITORY)
                ));
        return definitionSet;
    }
    
    

    @Override
    public JMadModelDefinition create() {
        // TODO Auto-generated method stub
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TT24T4");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        modelDefinition.setModelPathOffsets(offsets);

        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
       
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl tt24t4 = new SequenceDefinitionImpl("tt24t4",null);
        modelDefinition.setDefaultSequenceDefinition(tt24t4);
        RangeDefinitionImpl tt24t4range = new RangeDefinitionImpl(tt24t4, "ALL", createExtrInitialConditions());
        tt24t4.setDefaultRangeDefinition(tt24t4range);

        return modelDefinition;
        
        
      
    }
    
    /**
     * Twiss initial conditions from extraction point
     */
    private final TwissInitialConditionsImpl createExtrInitialConditions() {
        
        
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("extrsps-twiss");
        
        twissInitialConditions.setSaveBetaName("EXTR.INITBETA0");
       
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
      
        return twissInitialConditions;

    }

}
