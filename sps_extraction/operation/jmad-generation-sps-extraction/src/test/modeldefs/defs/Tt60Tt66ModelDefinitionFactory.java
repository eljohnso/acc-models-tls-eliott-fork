/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.TableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Tt60Tt66ModelDefinitionFactory implements ModelDefinitionFactory{
    
    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        
        /*
         * OPTICS
         */
        String[] strengthFileNames = new String[] {
                "tt60tt66_hiradmat/line/str/focus_fp1_0p1mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_0p2mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_0p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_0p7mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_1p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_2p0mm.str",               
                "tt60tt66_hiradmat/line/str/focus_fp1_2p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_3p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_3p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp1_4p0mm.str",               
                "tt60tt66_hiradmat/line/str/focus_fp2_0p1mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_0p2mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_0p25mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_0p3mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_0p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_0p7mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_1p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_1p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_2p0mm.str",               
                "tt60tt66_hiradmat/line/str/focus_fp2_2p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_3p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_3p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_4p0mm.str",           
                "tt60tt66_hiradmat/line/str/focus_fp3_0p1mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_0p2mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_0p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_0p7mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_1p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_1p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_2p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_2p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_3p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_3p5mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp3_4p0mm.str",              
                "tt60tt66_hiradmat/line/str/focus_fp1_10p0mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_1p0_0p34mm.str",
                "tt60tt66_hiradmat/line/str/focus_fp2_2p2_0p25mm.str",
                };
        

        for (String strengthFileName : strengthFileNames) {
            String opticsName = strengthFileName.replaceAll("tt60tt66_hiradmat/line/str/focus_", "tt66_2021_").replaceAll(".str", "");
            OpticsDefinition opticsDefinition = new OpticsDefinitionImpl(opticsName,
                    new ModelFile[] { new CallableModelFileImpl(strengthFileName, ModelFileLocation.REPOSITORY,
                            ParseType.STRENGTHS) });
            definitionSet.add(opticsDefinition);
        }
        
        
        
        return definitionSet;
    }
    
    
  
    @Override
    public JMadModelDefinition create() {
        // TODO Auto-generated method stub
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TT60TT66");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        modelDefinition.setModelPathOffsets(offsets);
        
        modelDefinition.addInitFile(new CallableModelFileImpl("operation/beta0.inp", ModelFileLocation.REPOSITORY));
        modelDefinition.addInitFile(new CallableModelFileImpl("tt60tt66_hiradmat/stitched/jmad/sps_tt66hiradmat_savedseq.seq", ModelFileLocation.REPOSITORY));
        modelDefinition.addInitFile(new CallableModelFileImpl("tt60tt66_hiradmat/stitched/jmad/sps_tt66_hiradmat.inp", ModelFileLocation.REPOSITORY));
        modelDefinition.addInitFile(new TableModelFileImpl("tt60tt66_hiradmat/stitched/jmad/sps_tt66_hiradmat_errors.seq", ModelFileLocation.REPOSITORY,"errtab"));
        
        
        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }
       
        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        /*
         * SEQUENCE
         */

       
        SequenceDefinitionImpl tt60tt66 = new SequenceDefinitionImpl("sps_tt66",null);
        modelDefinition.setDefaultSequenceDefinition(tt60tt66);
        RangeDefinitionImpl tt60tt66range = new RangeDefinitionImpl(tt60tt66, "ALL", createExtrInitialConditions());
        tt60tt66range.addPostUseFile(new CallableModelFileImpl("operation/seterrtab.madx",ModelFileLocation.REPOSITORY));
        tt60tt66.setDefaultRangeDefinition(tt60tt66range);

        return modelDefinition;
        
        
      
    }
    
    /**
     * Twiss initial conditions from extraction point
     */
    private final TwissInitialConditionsImpl createExtrInitialConditions() {
        
        
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("extrsps-twiss");
        
        twissInitialConditions.setSaveBetaName("EXTR.INITBETA0");
       
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
      
        return twissInitialConditions;

    }

}
