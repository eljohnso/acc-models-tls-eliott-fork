!====================================================
! MADX model of SPS-TT60-TI2 for LHC Q20 optics
!
! F.M.Velotti: TI2 optics as obtained from M.Fraser after
!              rematching for new TCDILs. 
!              SPS optics taken from SPS new repo
!              and added extraction bumps as in 
!              operation in 2018
!====================================================
 title, "SPS-TT60-TI2 LHC Q20 optics. Protons - 450 GeV/c";

 option, echo;
 option, RBARC=FALSE;

  set, format="22.10e";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

/* system,"[ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo"; */
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../sps_ext_elements sps_extr_repo";
system, "ln -fns ./../../tt60ti2 ti2_repo";

system, "ln -fns ./../line ti2_line_repo";

/***************************************
* TT60-TI2 line model
***************************************/

call,    file = "ti2_repo/ti2.seq";
call,    file = "ti2_repo/ti2_apertures.dbx";
call,    file = "ti2_line_repo/ti2_liu.str";

beam,    sequence=ti2, particle=proton, pc= 450;
use,     sequence=ti2;

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.5e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

! Errors on b2 and b3 as measured
call, file="ti2_repo/mbi_b3_error.madx";
! Call legacy initial conditons
call, file = "./sps_tt60_ti2_lhc_q20.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=Y0,
      PY=PY0;

};

exec, set_ini_conditions();

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file="twiss_ti2.tfs";

l_ti2 = table(twiss, LHCINJ.TI2, S);

/*****************************************************************************
Calculate extraction from SPS and prepare for stitching
*****************************************************************************/

call, file = "./load_lhc_q20_extraction.madx";

exec, calculate_stitched_absolute_trajecotry(twiss_sps_ti2_q20_nom.tfs, ti2);

! Save sequence for JMAD
exec, save_stitched_sequence(sps_ti2, q20);

/************************************
* Cleaning up
************************************/

system, "rm -rf sps_repo || rm sps_repo";
system, "rm ti2_line_repo";
system, "rm ti2_repo";
system, "rm sps_extr_repo";

