!==============================================================================================
! MADX file for PSB-BT3-BTM-BTY HRS optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "PSB/BT3/BTM/BTY HRS optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Cleaning .inp output files
***************************************/

system, "rm *.inp";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btm btm_repo";
system, "ln -fns ./../../bty bty_repo";

/*******************************************************************************
 * Beam command
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*******************************************************************************
 * Macros
 *******************************************************************************/
 
set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};

write_ini_conditions(xtlgeode, pxtlgeode, beamname,filename) : macro = {

betx0 = beamname->betx;
bety0 =  beamname->bety;

alfx0 = beamname->alfx;
alfy0 = beamname->alfy;

dx0 = beamname->dx;
dy0 = beamname->dy;

dpx0 = beamname->dpx - pxtlgeode/(beam->beta);
dpy0 = beamname->dpy;

x0 = beamname->x - xtlgeode;
y0 = beamname->y;

px0 = beamname->px - pxtlgeode;
py0 = beamname->py;

mux0 = beamname->mux;
muy0 = beamname->muy;

assign, echo="filename";

print, text="/*********************************************************************";
print, text="Initial conditions from MADX model of PSB extraction to BT lines";
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

value,x0 ;
value,px0 ;

assign, echo=terminal;

};


/*****************************************************************************
 Calculate initial conditions for BT transfer line
*****************************************************************************/

call, file = "bt_repo/load_psb3_extraction_isolde.madx";

! In this example the KFA14's kick can be adjusted (absolute error  in rad), sign and 
! file name to save the ring twiss 
! Returns all initial conditions needed, hence a set_ini_conditions() is needed 
! If "sign" (second argument) = 1, positive kick, negative otherwise

 set, format="22.10e";
exec, calculate_extraction(1e-3, 1, twiss_psb_stitched);    

/***********************************************************
* Define TL reference frame - to be updated with SU (GEODE) measured values
************************************************************/

! Until we have better information we assume the beam is aligned to BT in the nominal case
xtl = tl_initial_cond_nominal->x;
pxtl = tl_initial_cond_nominal->px;

/*****************************************************************************
 * BT3, BTM and BTY
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-LIU_dump.str";
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT3_LIU.seq";
 call, file = "bt_repo/BT.dbx";
 call, file = "btm_repo/BTM-ISOLDE.str";
 call, file = "btm_repo/BTM.ele";
 call, file = "btm_repo/BTM-LIU.seq"; 
 call, file = "btm_repo/BTM.dbx"; 
 call, file = "bty_repo/BTY_HRS.str";
 call, file = "bty_repo/BTY.ele";
 call, file = "bty_repo/BTY.seq"; 
 !call, file = "bty_repo/BTY.dbx";  ! Needs to be created
 option, echo;
 

 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/

lpsbext = 139.110102;

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtmbty = 10.84522;

lbty2ts2 = 110.9231473;

! BTY start marker at upstream flange of BTY.BVT10
EXTRACT, SEQUENCE=btm, FROM=BTM.START, TO=BTY.START, NEWNAME=btm2bty;

bt3btmbtyhrs: SEQUENCE, refer=ENTRY, l  =  lbt3 + lbtmbty + lbty2ts2;
	           bt3       , at =     0;
	           btm2bty    , at =  lbt3;
               bty2ts2    , at =  lbt3 + lbtmbty;              
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt3btmbtyhrs;
FLATTEN;
ENDEDIT;

/***********************************************************
* Save initial parameters in PSB ring for JMAD
************************************************************/
exec, write_ini_conditions(0,0,psbstart,PSB3_START_ISOLDE.inp);
exec, write_ini_conditions(0,0,smhstart,SMH3_START_ISOLDE.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond,BT3_START_KICK_RESPONSE_ISOLDE.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT3-BTM-BTY and stitch result for kick response
 *******************************************************************************/

use, sequence= bt3btmbtyhrs;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_kick_response, place = #e;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt3btmbty_hrs_kick_response_complete.tfs";

/***********************************************************
* Save parameters at target for kick response
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_kick_response,HRS_TARGET_KICK_RESPONSE_ISOLDE.inp);

/***********************************************************
* Initialise and save initial parameters to file for TL usage (reference frame modified)
************************************************************/

exec, write_ini_conditions(xtl,pxtl,tl_initial_cond_nominal,BT3_START_ISOLDE.inp);
exec, set_ini_conditions();

/*******************************************************************************
 * Run twiss for BT3-BTM-BTY and stitch result for nominal case
 *******************************************************************************/

use, sequence= bt3btmbtyhrs;  
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta,label=tl_final_cond_nominal, place = #e;
savebeta,label=bty3_start_isolde, place = bty.start;
twiss, beta0=initbeta0;

! Make one single tfs file for both ring and BT transfer line
len_twiss_tl = table(twiss, tablelength);

i = 1;
option, -info;
while(i < len_twiss_tl){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt3btmbty_hrs_nom_complete.tfs";

/***********************************************************
* Save parameters at BTY start for nominal case
************************************************************/

exec, write_ini_conditions(0,0,bty3_start_isolde,BTY3_START_ISOLDE.inp);

/***********************************************************
* Save parameters at target for kick response for nominal case
************************************************************/

exec, write_ini_conditions(0,0,tl_final_cond_nominal,HRS_TARGET_ISOLDE.inp);

/***********************************************************
* JMAD: prepare single sequences
************************************************************/

EXTRACT, SEQUENCE=psb3, FROM=PSB3.START, TO=BR3.BT_START, NEWNAME=psb3_ext;

psbbt3btmbtyhrs: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt3 + lbtmbty + lBTY2TS2;
    psb3_ext        , AT =  0.0000000000 ;
	bt3          	, AT =  lpsbext ;
    btm2bty          	, AT =  lpsbext + lbt3;  
    BTY2TS2          	, AT =  lpsbext + lbt3 + lbtmbty; 
ENDSEQUENCE;             

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

SEQEDIT, SEQUENCE = psbbt3btmbtyhrs;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR3.BT_START;
FLATTEN;
ENDEDIT;

! Special sequence for HE corrector knob upstream up BT.SMH15

EXTRACT, SEQUENCE=psb3, FROM=BR3.DVT14L1, TO=BR3.BT_START, NEWNAME=smh_ext;

lsmh_ext = lpsbext - 127.93409;

smhbt3btmbtyhrs: SEQUENCE, refer=ENTRY, L  = lsmh_ext + lbt3 + lbtmbty + lBTY2TS2;
    smh_ext        , AT =  0.0000000000 ;
	bt3            , AT =  lsmh_ext ;
    btm2bty         , AT =  lsmh_ext + lbt3;  
    BTY2TS2         , AT =  lsmh_ext + lbt3 + lbtmbty; 
ENDSEQUENCE;

SEQEDIT, SEQUENCE = smhbt3btmbtyhrs;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR3.BT_START;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE3KFA14L1 := kBE3KFA14L1REF;

 set, format="22.10e";
use, sequence= psbbt3btmbtyhrs;  
option, -warn;
save, sequence=psbbt3btmbtyhrs, beam, file='jmad/psbbt3btmbty_hrs.jmad';
option, warn;

 set, format="22.10e";
use, sequence= smhbt3btmbtyhrs;  
option, -warn;
save, sequence=smhbt3btmbtyhrs, beam, file='jmad/smhbt3btmbty_hrs.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bt3btmbtyhrs; 
option, -warn;
save, sequence=bt3btmbtyhrs, beam, file='jmad/bt3btmbty_hrs.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bty2ts2; 
option, -warn;
save, sequence=bty2ts2, beam, file='jmad/bty_hrs.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb3_start_isolde.inp jmad";
system, "cp smh3_start_isolde.inp jmad";
system, "cp bt3_start_isolde.inp jmad";
system, "cp bty3_start_isolde.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm bt_repo";
system, "rm btm_repo";
system, "rm bty_repo";
system, "rm -rf psb_repo || rm psb_repo";
stop;
