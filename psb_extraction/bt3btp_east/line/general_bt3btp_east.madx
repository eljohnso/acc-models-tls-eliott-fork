!==============================================================================================
! MADX file for BT3 EAST optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

 title, "BT3 optics";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";

/***************************************
* Load needed repos
***************************************/

system, "ln -fns ./../../bt bt_repo";
system, "ln -fns ./../../btp btp_repo";


/*******************************************************************************
 * Beam
 *******************************************************************************/
 
 BEAM, PARTICLE=PROTON, PC = 2.794987;
 BRHO := BEAM->PC * 3.3356;

/*****************************************************************************
 * BT3 and BTP
 * NB! The order of the .ele .str and .seq files matter.
 *
 *****************************************************************************/
 
 option, -echo;
 call, file = "bt_repo/BT-BTP-LIU.str";
 call, file = "bt_repo/LHC-LIU.str";
 !call, file = "bt_repo/HI-LIU.str"; ! uncomment for HI optics (also need PI.QLB)
 call, file = "bt_repo/BT.ele";
 call, file = "bt_repo/BT3_LIU.seq";
 call, file = "bt_repo/BT.dbx"; 
 call, file = "btp_repo/BTP.ele";
 call, file = "btp_repo/BTP.seq"; 
 call, file = "btp_repo/BTP.dbx"; 
 option, echo;

/*******************************************************************************
! Set initial twiss parameters
 *******************************************************************************/
 
call, file = "./../stitched/bt3_start_east.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0,
      X=X0,
      PX=PX0,
      Y=PY0,
      PY=PY0,
      PT=PT0;

};


exec, set_ini_conditions();


 /*******************************************************************************
 * Combine sequences
 *******************************************************************************/

lbt1 = 33.91783854;
lbt2 = 33.9071301;
lbt3 = 33.89615656;
lbt4 = 33.906865;

lbtp = 35.79972785;

bt3btp: SEQUENCE, refer=ENTRY, L  = lbt3 + lbtp;
	BT3, 	AT = 0 ;
	BTP, 	AT = lbt3;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt3btp;
FLATTEN;
ENDEDIT;

/*******************************************************************************
 * Twiss
 *******************************************************************************/

use, sequence = bt3btp; 
SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file = "twiss_bt3btp_east_nom.tfs";


/*************************************
* Survey
*************************************/
set_su_ini_conditions(xx) : macro = {

 x00 = table(survey,xx,X);
 y00 = table(survey,xx,Y);
 z00 = table(survey,xx,Z);
 theta00 = table(survey,xx,THETA);
 phi00 = table(survey,xx,PHI);
 psi00 = table(survey,xx,PSI);

 };
 
call, file = "./make_survey_bt3btp.madx"; 

/***********************************
* Cleaning up
***********************************/
system, "rm bt_repo";
system, "rm btp_repo";

stop;
