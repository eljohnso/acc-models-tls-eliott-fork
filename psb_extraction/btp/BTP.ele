!--------------------------------------------------------------------------------------
! Element definition file for BTP (LIU optics)
!--------------------------------------------------------------------------------------
! Changes Feb 17 (C. Hessler):
! - length BT.BHZ10 2.2m -> 2.0m
! - length BPMs 0.185m/0.219m -> 0.328m
! - small shifts of some correctors to avoid collisions with the enlarged BPMs
!
! Changes 17/10/2017 (C. Hessler):
! - Move strengths for BT.BHZ10 and SMH42 to strength file
!
! Changes 18/12/2017 (C. Hessler):
! - Re-integration of BTP.QNO10
!
! Changes 14/11/2018 (C. Hessler)
! - keep prensetly installed elements in the wall (BPM00, DHZ10, DVT10, QNO10) and do
!   not exchange them as originally planned.
! - Replace BPM respresentation in sequence file:
!   BPM with real flange-to-flange length -> BPM with length 0 (at center of electrodes)
!
! Changes 29/5/2020 (S. Ogur)
! - corrected length of BTP by + 0.018645 m to keep BT + BTP lines total length same as the pre-LS2
! - moved SMH42 -0.004585 m to match drawings
! - fixed bug in stray field implementation that slipped by one index shifted by -0.009157
!--------------------------------------------------------------------------------------


!magnetic length of the new quads based on EDMS 1549299
lm=0.640;

!placeholders to define the entry of the elements
! BTP.QNO10.E:   placeholder;
! btp.qno20.E:   placeholder;
! btp.qno30.E:   placeholder;
! btp.qno35.E:   placeholder;
! btp.qno50.E:   placeholder;
! btp.qno55.E:   placeholder;
! btp.qno60.E:   placeholder;

BTP.STP05 : MARKER; 
BTP.STP05.ENTRYFACE : MARKER;
BTP.STP10 : MARKER; 
BTP.STP10.ENTRYFACE : MARKER;

!btp.BPM00.E:   placeholder;
!btp.BPM10.E:   placeholder;
!btp.BPM20.E:   placeholder;
!btp.BPM30.E:   placeholder;
!BTP.BPMW15.E: placeholder;
!BTP.BPM60.E: placeholder;

!btp.vvs10.E:   placeholder;


!placeholders to define the exit (sortie) of the elements
!BTP.QNO10.S:   placeholder;
!btp.qno20.S:   placeholder;
!btp.qno30.S:   placeholder;
!btp.qno35.S:   placeholder;
!btp.qno50.S:   placeholder;
!btp.qno55.S:   placeholder;
!btp.qno60.S:   placeholder;

!BTP.STP.S: placeholder;

! btp.BPM00.S:   placeholder;
! btp.BPM10.S:   placeholder;
! btp.BPM20.S:   placeholder;
! btp.BPM30.S:   placeholder;
! BTP.BPMW15.S: placeholder;
! BTP.BPM60.S: placeholder;

! btp.vvs10.S:   placeholder;

bt.bhz10: sbend,l:= 2.000457493,angle:= btbhz10.angle  ,e1:= 0,e2:= btbhz10.angle,hgap:= 0.07,fint:= 0.3;
// The magnetic length has been reduced from 2.2 m (path length) to 2.000457 m (path length).
// To keep the downstream elements at the same survey coordinates,
// the increased path length in the arrangement with the shortened BHZ10 must be taken into account:
// Therefore BT.BHZ10 must be shifted downstream by btbhz10.ds and all following elements by 2*btbhz10.ds.
btbhz10.ds=0.099668699/cos(btbhz10.angle/2)-(btbhz10.angle/2)*0.099668699/sin(btbhz10.angle/2);

// Explanation: Lmag(old)=2.2/btbhz10.angle*2*sin(btbhz10.angle/2)=2.197738627727103
//		Lmag(new)=2.000457493/btbhz10.angle*2*sin(btbhz10.angle/2)=1.998401228
//              (2.197738627727103 - 1.998401228)/2 = 0.099668699
//		For calculation of ds see file BTBHZ10-length-change-ds-calculation-new.pdf


btp.vvs10: INSTRUMENT, L:=0.1;! L changed to 0.1, CH 30/08/2017
btp.stp: instrument,l:= 1.457;
btp.stp_face: marker;
btp.BPM00: monitor,l:= 0;
btp.dhz10: hkicker,l:= 0.436,kick:=kbtpdhz10;! DHZ10 will be kept and not replaced by type 1 dual plane kicker, CH 14/11/2018
btp.dvt10: vkicker,l:= 0.436,kick:=kbtpdvt10;! DVT10 will be kept and not replaced by type 1 dual plane kicker, CH 14/11/2018
btp.qno10: quadrupole,l:= 0.465,k1:=kbtpqno10;! QNO10 will be kept and not replaced by the air cooled Triumf quadrupole. CH 14/11/2018
btp.qno20: quadrupole,l:= lm,k1:=kbtpqno20;
btp.dhz20: hkicker,l:= 0.436;
btp.BPM10: monitor,l:= 0;
btp.dvt20: vkicker,l:= 0.436;
btp.qno30: quadrupole,l:= lm,k1:=kbtpqno30;
BTP.BPMW15:monitor, L:=0;
btp.qno35: quadrupole,l:= lm,k1:=kbtpqno35;
btp.dhz30: hkicker,l:= 0.436;
btp.BPM20: monitor,l:= 0;
btp.dvt30: vkicker,l:= 0.436;
btp.qno50: quadrupole,l:= lm,k1:=kbtpqno50;
btp_hcoll: rcollimator,l:= 1;
btp.qno55: quadrupole,l:= lm,k1:=kbtpqno55;
btp_vcoll: rcollimator,l:= 1;
btp.dvt40: vkicker,l:= 0.436;
BTP.BCT10: INSTRUMENT, L:=0.522;! L changed to 0.522, CH 30/08/2017
btp.qno60: quadrupole,l:= lm,k1:=kbtpqno60 ;
BTP.VVS20: INSTRUMENT, L:=0.1;! L changed to 0.1, CH 30/08/2017
btp.BPM30: monitor,l:= 0;
btp.dhz40: hkicker,l:= 0.436;
btp.dvt50: vkicker,l:= 0.436;
btp.BPM60: monitor,l:= 0;

! S. Ogur, W. Bartmann: stray field #1 was set to zero then all stray were shifted in the old post-LS2 file, now correct version

stray1 : multipole,knl:={2.11E-07,-3.11E-06,5.25E-05,-0.001056026};
stray2 : multipole,knl:={2.78E-07,-4.03E-06,6.74E-05,-0.001303161};
stray3 : multipole,knl:={3.54E-07,-5.09E-06,8.47E-05,-0.001548501};
stray4 : multipole,knl:={4.40E-07,-6.29E-06,0.00010422,-0.001801833};
stray5 : multipole,knl:={5.35E-07,-7.62E-06,0.000126011,-0.002083152};
stray6 : multipole,knl:={6.39E-07,-9.08E-06,0.000149757,-0.002396463};
stray7 : multipole,knl:={7.51E-07,-1.06E-05,0.000175017,-0.002735335};
stray8 : multipole,knl:={8.70E-07,-1.23E-05,0.000201271,-0.003092501};
stray9 : multipole,knl:={9.95E-07,-1.40E-05,0.000227928,-0.003465212};
stray10 : multipole,knl:={1.12E-06,-1.57E-05,0.000254586,-0.003849371};
stray11 : multipole,knl:={1.26E-06,-1.75E-05,0.000280859,-0.004285322};
stray12 : multipole,knl:={1.39E-06,-1.92E-05,0.000306441,-0.004796871};
stray13 : multipole,knl:={1.53E-06,-2.09E-05,0.000331182,-0.005257346};
stray14 : multipole,knl:={1.67E-06,-2.27E-05,0.000354988,-0.005610906};
stray15 : multipole,knl:={1.80E-06,-2.44E-05,0.000377816,-0.005909807};
stray16 : multipole,knl:={1.93E-06,-2.60E-05,0.000400019,-0.006190373};
stray17 : multipole,knl:={2.07E-06,-2.77E-05,0.000422381,-0.00652635};
stray18 : multipole,knl:={2.20E-06,-2.93E-05,0.000445275,-0.0069502};
stray19 : multipole,knl:={2.32E-06,-3.09E-05,0.000468895,-0.007439421};
stray20 : multipole,knl:={2.45E-06,-3.24E-05,0.000493753,-0.007961406};
stray21 : multipole,knl:={2.57E-06,-3.39E-05,0.000517992,-0.00848414};
stray22 : multipole,knl:={2.69E-06,-3.54E-05,0.000540398,-0.008998817};
stray23 : multipole,knl:={2.81E-06,-3.68E-05,0.000562329,-0.009529896};
stray24 : multipole,knl:={2.92E-06,-3.83E-05,0.000584466,-0.010102745};
stray25 : multipole,knl:={3.04E-06,-3.98E-05,0.000607107,-0.010612356};
stray26 : multipole,knl:={3.16E-06,-4.13E-05,0.000630275,-0.010964631};
stray27 : multipole,knl:={3.28E-06,-4.29E-05,0.000654142,-0.011173737};
stray28 : multipole,knl:={3.40E-06,-4.45E-05,0.000679046,-0.011268153};
stray29 : multipole,knl:={3.52E-06,-4.62E-05,0.000705198,-0.011331118};
stray30 : multipole,knl:={3.64E-06,-4.80E-05,0.0007327,-0.011511907};
stray31 : multipole,knl:={3.76E-06,-4.97E-05,0.000761658,-0.011880709};
stray32 : multipole,knl:={3.89E-06,-5.15E-05,0.00079242,-0.012363038};
stray33 : multipole,knl:={4.01E-06,-5.33E-05,0.000824597,-0.012890383};
stray34 : multipole,knl:={4.14E-06,-5.52E-05,0.000856974,-0.013454681};
stray35 : multipole,knl:={4.28E-06,-5.72E-05,0.000889262,-0.014091086};
stray36 : multipole,knl:={4.41E-06,-5.92E-05,0.000921747,-0.014787237};
stray37 : multipole,knl:={4.56E-06,-6.13E-05,0.000954523,-0.015481062};
stray38 : multipole,knl:={4.71E-06,-6.35E-05,0.000988276,-0.016117983};
stray39 : multipole,knl:={4.86E-06,-6.58E-05,0.001023993,-0.016667724};
stray40 : multipole,knl:={5.01E-06,-6.82E-05,0.001061993,-0.017103787};
stray41 : multipole,knl:={5.17E-06,-7.07E-05,0.001102246,-0.017430017};
stray42 : multipole,knl:={5.33E-06,-7.33E-05,0.001142466,-0.017723245};
stray43 : multipole,knl:={5.50E-06,-7.58E-05,0.001182069,-0.018069813};
stray44 : multipole,knl:={5.66E-06,-7.84E-05,0.001222914,-0.018473747};
stray45 : multipole,knl:={5.84E-06,-8.10E-05,0.00126477,-0.018912147};
stray46 : multipole,knl:={6.01E-06,-8.38E-05,0.001307311,-0.019407332};
stray47 : multipole,knl:={6.20E-06,-8.66E-05,0.001350661,-0.019937972};
stray48 : multipole,knl:={6.39E-06,-8.96E-05,0.001394791,-0.020442201};
stray49 : multipole,knl:={6.59E-06,-9.26E-05,0.001439432,-0.020898108};
stray50 : multipole,knl:={6.79E-06,-9.58E-05,0.001484509,-0.021377745};
stray51 : multipole,knl:={7.00E-06,-9.90E-05,0.001530362,-0.02188865};
stray52 : multipole,knl:={7.22E-06,-0.000102381,0.001577398,-0.022325122};
stray53 : multipole,knl:={7.44E-06,-0.00010585,0.001625438,-0.022661257};
stray54 : multipole,knl:={7.67E-06,-0.000109427,0.001674343,-0.022949155};
stray55 : multipole,knl:={7.91E-06,-0.000113112,0.001724233,-0.023201738};
stray56 : multipole,knl:={8.16E-06,-0.000116908,0.001774744,-0.023394661};
stray57 : multipole,knl:={8.42E-06,-0.000120821,0.001825432,-0.023539208};
stray58 : multipole,knl:={8.68E-06,-0.000124844,0.001876144,-0.023680439};
stray59 : multipole,knl:={8.95E-06,-0.000128979,0.001927116,-0.023792642};
stray60 : multipole,knl:={9.24E-06,-0.000133236,0.001978306,-0.023815263};
stray61 : multipole,knl:={9.53E-06,-0.000137605,0.002029338,-0.023769589};
stray62 : multipole,knl:={9.83E-06,-0.000142111,0.002080877,-0.023715076};
stray63 : multipole,knl:={1.01E-05,-0.000146664,0.002132032,-0.023640493};
stray64 : multipole,knl:={1.04E-05,-0.000151173,0.002180767,-0.023473336};
stray65 : multipole,knl:={1.08E-05,-0.000155717,0.002227834,-0.023226718};
stray66 : multipole,knl:={1.11E-05,-0.000160352,0.002274397,-0.022960095};
stray67 : multipole,knl:={1.14E-05,-0.000165085,0.002320477,-0.022661698};
stray68 : multipole,knl:={1.17E-05,-0.000169916,0.002365938,-0.022293973};
stray69 : multipole,knl:={1.21E-05,-0.000174845,0.002410479,-0.021866631};
stray70 : multipole,knl:={1.25E-05,-0.000179864,0.002454074,-0.021427349};
stray71 : multipole,knl:={1.28E-05,-0.00018497,0.002496942,-0.020974274};
stray72 : multipole,knl:={1.32E-05,-0.000190171,0.00253898,-0.020463832};
stray73 : multipole,knl:={1.36E-05,-0.000195463,0.002579979,-0.019905154};
stray74 : multipole,knl:={1.40E-05,-0.00020084,0.002619914,-0.019355392};
stray75 : multipole,knl:={1.44E-05,-0.000206301,0.002658967,-0.01882108};
stray76 : multipole,knl:={1.48E-05,-0.000211845,0.002697187,-0.018258687};
stray77 : multipole,knl:={1.53E-05,-0.000217474,0.002734311,-0.017670863};
stray78 : multipole,knl:={1.57E-05,-0.000223175,0.002770354,-0.017110861};
stray79 : multipole,knl:={1.62E-05,-0.000228946,0.002805442,-0.016593658};
stray80 : multipole,knl:={1.67E-05,-0.000234791,0.00283963,-0.016078651};
stray81 : multipole,knl:={1.72E-05,-0.000240703,0.002872837,-0.015562148};
stray82 : multipole,knl:={1.77E-05,-0.000246678,0.002905041,-0.015100104};
stray83 : multipole,knl:={1.82E-05,-0.000252711,0.002936539,-0.014711006};
stray84 : multipole,knl:={1.87E-05,-0.000258828,0.002968898,-0.014370791};
stray85 : multipole,knl:={1.92E-05,-0.000264819,0.002999686,-0.014061585};
stray86 : multipole,knl:={1.97E-05,-0.000270654,0.003027581,-0.013814644};
stray87 : multipole,knl:={2.02E-05,-0.000276534,0.003055143,-0.01366298};
stray88 : multipole,knl:={2.08E-05,-0.00028246,0.003082548,-0.013566637};
stray89 : multipole,knl:={2.13E-05,-0.000288436,0.003109795,-0.01350394};
stray90 : multipole,knl:={2.19E-05,-0.000294458,0.003136932,-0.013510627};
stray91 : multipole,knl:={2.24E-05,-0.000300521,0.003164198,-0.013627556};
stray92 : multipole,knl:={2.30E-05,-0.000306633,0.003191809,-0.013828565};
stray93 : multipole,knl:={2.36E-05,-0.000312797,0.003220011,-0.014086144};
stray94 : multipole,knl:={2.42E-05,-0.000319019,0.003249097,-0.014437697};
stray95 : multipole,knl:={2.48E-05,-0.000325296,0.003279105,-0.014904328};
stray96 : multipole,knl:={2.54E-05,-0.000331635,0.003310095,-0.015448998};
stray97 : multipole,knl:={2.61E-05,-0.000338044,0.003342276,-0.016027422};
stray98 : multipole,knl:={2.67E-05,-0.00034452,0.003375923,-0.016670642};
stray99 : multipole,knl:={2.74E-05,-0.00035106,0.003411651,-0.017471803};
stray100 : multipole,knl:={2.80E-05,-0.000357659,0.003449653,-0.018439842};
stray101 : multipole,knl:={2.87E-05,-0.000364324,0.00348992,-0.019495007};
stray102 : multipole,knl:={2.94E-05,-0.000371054,0.003532336,-0.020599162};
stray103 : multipole,knl:={3.01E-05,-0.00037784,0.003577032,-0.021792317};
stray104 : multipole,knl:={3.08E-05,-0.000384682,0.003624559,-0.023064976};
stray105 : multipole,knl:={3.15E-05,-0.000391639,0.003676748,-0.024426861};
stray106 : multipole,knl:={3.21E-05,-0.000398429,0.003731366,-0.025915494};
stray107 : multipole,knl:={3.28E-05,-0.000404909,0.003786744,-0.027563565};
stray108 : multipole,knl:={3.34E-05,-0.000411231,0.003844885,-0.029413098};
stray109 : multipole,knl:={3.40E-05,-0.000417282,0.003904932,-0.031384739};
stray110 : multipole,knl:={3.45E-05,-0.000422859,0.003964842,-0.03342265};
stray111 : multipole,knl:={3.50E-05,-0.000427637,0.0040191,-0.035497428};
stray112 : multipole,knl:={3.55E-05,-0.000431156,0.004057385,-0.037503954};
stray113 : multipole,knl:={3.58E-05,-0.000432812,0.004063931,-0.039112974};
stray114 : multipole,knl:={3.60E-05,-0.000431982,0.004017714,-0.039664147};
stray115 : multipole,knl:={3.61E-05,-0.000428196,0.003897024,-0.038357766};
stray116 : multipole,knl:={3.61E-05,-0.000421241,0.003691907,-0.034718124};
stray117 : multipole,knl:={3.59E-05,-0.00041152,0.003402346,-0.02833081};
stray118 : multipole,knl:={3.57E-05,-0.000400093,0.003061126,-0.02035796};
stray119 : multipole,knl:={3.55E-05,-0.000388326,0.002721161,-0.012765379};
stray120 : multipole,knl:={3.53E-05,-0.000377511,0.002419013,-0.006493581};
stray121 : multipole,knl:={3.52E-05,-0.000368548,0.002178326,-0.002117085};
stray122 : multipole,knl:={3.51E-05,-0.00036183,0.002002935,0.000485143};
stray123 : multipole,knl:={3.52E-05,-0.000357311,0.001883698,0.001762285};
stray124 : multipole,knl:={3.54E-05,-0.000354742,0.001808067,0.002162153};
stray125 : multipole,knl:={3.56E-05,-0.00035377,0.001763152,0.002057555};
stray126 : multipole,knl:={3.60E-05,-0.000354012,0.001737871,0.001741295};
stray127 : multipole,knl:={3.64E-05,-0.000355187,0.001725216,0.001344716};
stray128 : multipole,knl:={3.68E-05,-0.000356921,0.001720175,0.000895923};
stray129 : multipole,knl:={3.73E-05,-0.000358958,0.00171897,0.000420812};
stray130 : multipole,knl:={3.78E-05,-0.000361272,0.001720018,-8.00E-05};
stray131 : multipole,knl:={3.83E-05,-0.000363783,0.001722403,-0.00058051};
stray132 : multipole,knl:={3.88E-05,-0.000366422,0.001725529,-0.001047551};
stray133 : multipole,knl:={3.94E-05,-0.000369147,0.001729049,-0.001475982};
stray134 : multipole,knl:={4.00E-05,-0.00037193,0.0017327,-0.001865901};
stray135 : multipole,knl:={4.06E-05,-0.000374753,0.001736479,-0.00221347};
stray136 : multipole,knl:={4.12E-05,-0.000377608,0.001740595,-0.002528264};
stray137 : multipole,knl:={4.18E-05,-0.000380488,0.00174521,-0.002843956};
stray138 : multipole,knl:={4.24E-05,-0.000383389,0.001750388,-0.003213346};
stray139 : multipole,knl:={4.30E-05,-0.000386313,0.001756403,-0.003624832};
stray140 : multipole,knl:={4.37E-05,-0.000389263,0.001763497,-0.004037156};
stray141 : multipole,knl:={4.43E-05,-0.000392238,0.001771572,-0.004433065};
stray142 : multipole,knl:={4.50E-05,-0.000395234,0.001780555,-0.004806783};
stray143 : multipole,knl:={4.56E-05,-0.000398246,0.001790341,-0.005181135};
stray144 : multipole,knl:={4.63E-05,-0.00040127,0.001800672,-0.0055701};
stray145 : multipole,knl:={4.70E-05,-0.000404301,0.001811476,-0.005980705};
stray146 : multipole,knl:={4.76E-05,-0.000407338,0.001822724,-0.006423419};
stray147 : multipole,knl:={4.83E-05,-0.000410382,0.001834282,-0.00688851};
stray148 : multipole,knl:={4.90E-05,-0.000413478,0.001846612,-0.007363452};
stray149 : multipole,knl:={4.97E-05,-0.000416578,0.001859862,-0.007841619};
stray150 : multipole,knl:={5.04E-05,-0.000419569,0.00187339,-0.008313319};
stray151 : multipole,knl:={5.10E-05,-0.000422499,0.001887328,-0.008788699};
stray152 : multipole,knl:={5.17E-05,-0.00042544,0.001902075,-0.009287905};
stray153 : multipole,knl:={5.23E-05,-0.000428388,0.001917472,-0.009821265};
stray154 : multipole,knl:={5.30E-05,-0.000431346,0.001933266,-0.010366489};
stray155 : multipole,knl:={5.37E-05,-0.000434317,0.001949474,-0.01090411};
stray156 : multipole,knl:={5.44E-05,-0.000437304,0.001966278,-0.011450298};
stray157 : multipole,knl:={5.51E-05,-0.00044031,0.001983766,-0.012013832};
stray158 : multipole,knl:={5.57E-05,-0.000443349,0.002002255,-0.012596929};
stray159 : multipole,knl:={5.64E-05,-0.000446433,0.002022175,-0.013219627};
stray160 : multipole,knl:={5.71E-05,-0.000449574,0.002043885,-0.013911385};
stray161 : multipole,knl:={5.79E-05,-0.000452787,0.002067524,-0.014656149};
stray162 : multipole,knl:={5.86E-05,-0.000456072,0.00209299,-0.015423878};
stray163 : multipole,knl:={5.93E-05,-0.00045942,0.002120209,-0.016225653};
stray164 : multipole,knl:={6.00E-05,-0.000462814,0.002148824,-0.017060536};
stray165 : multipole,knl:={6.08E-05,-0.000466241,0.002178574,-0.017920098};
stray166 : multipole,knl:={6.15E-05,-0.000469693,0.002209454,-0.018819669};
stray167 : multipole,knl:={6.22E-05,-0.000473165,0.002241243,-0.019772871};
stray168 : multipole,knl:={6.30E-05,-0.000476672,0.002273875,-0.020744248};
stray169 : multipole,knl:={6.37E-05,-0.000480225,0.002307652,-0.021710402};
stray170 : multipole,knl:={6.45E-05,-0.000483926,0.002344055,-0.022714683};
stray171 : multipole,knl:={6.52E-05,-0.000487507,0.002380541,-0.023711771};
stray172 : multipole,knl:={6.59E-05,-0.000490864,0.002415956,-0.024683793};
stray173 : multipole,knl:={6.66E-05,-0.000494257,0.002453014,-0.025703352};
stray174 : multipole,knl:={6.73E-05,-0.000497681,0.002491379,-0.026784864};
stray175 : multipole,knl:={6.80E-05,-0.000501133,0.002530559,-0.027894284};
stray176 : multipole,knl:={6.87E-05,-0.000504621,0.002570412,-0.02898413};
stray177 : multipole,knl:={6.94E-05,-0.000508141,0.002610958,-0.0300555};
stray178 : multipole,knl:={7.01E-05,-0.000511696,0.002652171,-0.031118525};
stray179 : multipole,knl:={7.09E-05,-0.000515297,0.002694146,-0.032159801};
stray180 : multipole,knl:={7.16E-05,-0.000518981,0.002737945,-0.033204457};
stray181 : multipole,knl:={7.23E-05,-0.000522787,0.002785066,-0.034326647};
stray182 : multipole,knl:={7.31E-05,-0.000526738,0.002836182,-0.035576027};
stray183 : multipole,knl:={7.38E-05,-0.000530848,0.002890958,-0.036895888};
stray184 : multipole,knl:={7.46E-05,-0.000535088,0.002948168,-0.038200137};
stray185 : multipole,knl:={7.54E-05,-0.000539418,0.003006637,-0.039462729};
stray186 : multipole,knl:={7.61E-05,-0.00054381,0.00306586,-0.040682209};
stray187 : multipole,knl:={7.69E-05,-0.000548252,0.003125924,-0.04186043};
stray188 : multipole,knl:={7.77E-05,-0.000552746,0.003187308,-0.043027949};
stray189 : multipole,knl:={7.85E-05,-0.000557291,0.003250017,-0.044226595};
stray190 : multipole,knl:={7.93E-05,-0.000561902,0.00331368,-0.045439195};
stray191 : multipole,knl:={8.01E-05,-0.00056669,0.003379373,-0.046625438};
stray192 : multipole,knl:={8.09E-05,-0.000571432,0.003444367,-0.04771804};
stray193 : multipole,knl:={8.16E-05,-0.000575971,0.003506888,-0.048702165};
stray194 : multipole,knl:={8.24E-05,-0.000580527,0.003569885,-0.049631518};
stray195 : multipole,knl:={8.31E-05,-0.000585162,0.003634311,-0.050510104};
stray196 : multipole,knl:={8.39E-05,-0.000589865,0.003700144,-0.051358974};
stray197 : multipole,knl:={8.46E-05,-0.000594609,0.003766724,-0.052216425};
stray198 : multipole,knl:={8.54E-05,-0.000599378,0.003832854,-0.053058415};
stray199 : multipole,knl:={8.62E-05,-0.000604167,0.003897336,-0.053796449};
stray200 : multipole,knl:={8.69E-05,-0.000608989,0.003959575,-0.054365581};
stray201 : multipole,knl:={8.77E-05,-0.000613897,0.004020381,-0.054767136};
stray202 : multipole,knl:={8.85E-05,-0.000619,0.004083126,-0.055082426};
stray203 : multipole,knl:={8.93E-05,-0.000624415,0.004152339,-0.055447223};
stray204 : multipole,knl:={9.01E-05,-0.000630149,0.004228859,-0.055906026};
stray205 : multipole,knl:={9.09E-05,-0.000636111,0.004308969,-0.056376102};
stray206 : multipole,knl:={9.18E-05,-0.000642189,0.004388132,-0.056752656};
stray207 : multipole,knl:={9.26E-05,-0.000648325,0.004464112,-0.056949207};
stray208 : multipole,knl:={9.34E-05,-0.000654531,0.004537557,-0.056962257};
stray209 : multipole,knl:={9.43E-05,-0.000660834,0.004609877,-0.056853144};
stray210 : multipole,knl:={9.51E-05,-0.000667284,0.004681813,-0.056605848};
stray211 : multipole,knl:={9.59E-05,-0.00067392,0.004753946,-0.056190866};
stray212 : multipole,knl:={9.67E-05,-0.000680792,0.004827522,-0.055602827};
stray213 : multipole,knl:={9.75E-05,-0.0006879,0.004903591,-0.054884303};
stray214 : multipole,knl:={9.82E-05,-0.000695128,0.004981479,-0.054074636};
stray215 : multipole,knl:={9.89E-05,-0.000702545,0.005063867,-0.053136357};
stray216 : multipole,knl:={9.95E-05,-0.000710282,0.005157234,-0.052054242};
stray217 : multipole,knl:={9.99E-05,-0.000718163,0.005268836,-0.050996073};
stray218 : multipole,knl:={0.000100144,-0.000725656,0.005407342,-0.050286409};
stray219 : multipole,knl:={0.000100009,-0.000731478,0.00557681,-0.050435231};
stray220 : multipole,knl:={9.93E-05,-0.0007329,0.005761365,-0.052150768};
stray221 : multipole,knl:={9.78E-05,-0.000724991,0.005892828,-0.055585697};
stray222 : multipole,knl:={9.53E-05,-0.000700681,0.005815977,-0.058946086};
stray223 : multipole,knl:={9.14E-05,-0.000653319,0.005318734,-0.057316771};
stray224 : multipole,knl:={8.62E-05,-0.000581277,0.004290933,-0.046457182};
stray225 : multipole,knl:={8.00E-05,-0.000494426,0.00294036,-0.029327468};
stray226 : multipole,knl:={7.33E-05,-0.000407384,0.001649107,-0.013940983};
stray227 : multipole,knl:={6.64E-05,-0.000329361,0.00064089,-0.004292524};
stray228 : multipole,knl:={5.95E-05,-0.000265123,-2.55E-05,0.000114161};
stray229 : multipole,knl:={5.29E-05,-0.000215135,-0.000393393,0.00122852};
stray230 : multipole,knl:={4.66E-05,-0.000176294,-0.000567855,0.000967123};
stray231 : multipole,knl:={4.05E-05,-0.000145834,-0.00062752,0.000240475};
stray232 : multipole,knl:={3.46E-05,-0.000121594,-0.000621093,-0.0003544};
stray233 : multipole,knl:={2.91E-05,-0.000101663,-0.000577223,-0.000709158};
stray234 : multipole,knl:={2.39E-05,-8.49E-05,-0.000510004,-0.000744041};
stray235 : multipole,knl:={1.91E-05,-7.02E-05,-0.000430219,-0.000526467};
stray236 : multipole,knl:={1.48E-05,-5.71E-05,-0.000342776,-0.000159002};
stray237 : multipole,knl:={1.11E-05,-4.52E-05,-0.000255262,0.000210884};
stray238 : multipole,knl:={8.01E-06,-3.46E-05,-0.000178161,0.000446146};
stray239 : multipole,knl:={5.59E-06,-2.57E-05,-0.000116826,0.000516768};
stray240 : multipole,knl:={3.77E-06,-1.86E-05,-7.29E-05,0.000472646};
stray241 : multipole,knl:={2.46E-06,-1.30E-05,-4.41E-05,0.000158728};
stray242 : multipole,knl:={1.56E-06,-9.13E-06,-2.75E-05,6.64E-05};

PI.SMH42.btp: rbend,l:= 0.942,angle:= angleSMH42;

/*
PI.SMH42.btp_chopped1: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped2: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped3: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped4: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped5: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped6: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped7: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped8: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped9: rbend,l:= 0.1,angle:= (angleSMH42/9.42);
PI.SMH42.btp_chopped10: rbend,l:= 0.042/2,angle:= (angleSMH42/9.42)*0.42/2;
PI.SMH42.btp_chopped11: rbend,l:= 0.042/2,angle:= (angleSMH42/9.42)*0.42/2;*/
