package modeldefs;

import java.io.File;

import org.apache.log4j.BasicConfigurator;

import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.io.JMadModelDefinitionExportRequest;
import cern.accsoft.steering.jmad.modeldefs.io.ModelDefinitionPersistenceService;
import cern.accsoft.steering.jmad.modeldefs.io.impl.XmlModelDefinitionPersistenceService;
import modeldefs.defs.Ti2LhcModelDefinitionFactory;
import modeldefs.defs.Ti8LhcModelDefinitionFactory;

public class LhcInjectionModelDefCreator {

    public static void main(String[] args) {
        BasicConfigurator.configure();

        ModelDefinitionFactory[] factories = new ModelDefinitionFactory[] { //
                new Ti2LhcModelDefinitionFactory(), new Ti8LhcModelDefinitionFactory(), };

        ModelDefinitionPersistenceService xmlService = new XmlModelDefinitionPersistenceService();

        for (ModelDefinitionFactory factory : factories) {
            JMadModelDefinition modelDefinition = factory.create();
            JMadModelDefinitionExportRequest exportRequest = JMadModelDefinitionExportRequest.allFrom(modelDefinition);
            try {
                File xmlFile = new File("../" + modelDefinition.getName().toLowerCase() + ".jmd.xml");
                System.out.println("Writing file '" + xmlFile.getAbsolutePath() + "'.");
                xmlService.save(exportRequest, xmlFile);
            } catch (Exception e) {
                System.out.println("Could not save model definition to file !");
                e.printStackTrace();
            }
        }
    }

}
