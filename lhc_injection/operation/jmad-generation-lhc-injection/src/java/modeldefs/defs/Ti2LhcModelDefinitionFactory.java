/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package modeldefs.defs;

import static modeldefs.defs.LhcTransferDefinitions.initialConditions;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.file.TableModelFileImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

public class Ti2LhcModelDefinitionFactory implements ModelDefinitionFactory {
    @Override
    public JMadModelDefinition create() {
        JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
        modelDefinition.setName("TI2LHCB1");

        ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
        offsets.setRepositoryPrefix("..");
        offsets.setResourcePrefix(".");
        modelDefinition.setModelPathOffsets(offsets);

        for (OpticsDefinition opticsDefinition : createOpticsDefinitions()) {
            modelDefinition.addOpticsDefinition(opticsDefinition);
        }

        modelDefinition.setDefaultOpticsDefinition(modelDefinition.getOpticsDefinitions().get(0));

        SequenceDefinitionImpl sequence = new SequenceDefinitionImpl("ti2lhcb1", null);
        modelDefinition.setDefaultSequenceDefinition(sequence);
        RangeDefinitionImpl allRange = new RangeDefinitionImpl(sequence, "ALL", initialConditions());
        sequence.setDefaultRangeDefinition(allRange);

        return modelDefinition;
    }

    private Set<OpticsDefinition> createOpticsDefinitions() {
        Set<OpticsDefinition> definitionSet = new HashSet<>();
        definitionSet.add(new OpticsDefinitionImpl("TI2LHCB1-LHC-Q20-2021v1",
                new CallableModelFileImpl("beta0.inp", ModelFileLocation.RESOURCE),
                new CallableModelFileImpl("ti2_q20/stitched/jmad/ti2lhcb1_q20.inp", ModelFileLocation.REPOSITORY),
                new CallableModelFileImpl("ti2_q20/stitched/jmad/ti2lhcb1q20_savedseq.seq",
                        ModelFileLocation.REPOSITORY),
                new TableModelFileImpl("ti2_q20/stitched/jmad/ti2lhcb1_q20_errors.seq", ModelFileLocation.REPOSITORY,
                        "errtab")));
        return definitionSet;
    }

}
